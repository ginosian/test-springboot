package com.margin.port.out.elastic.repository;

import com.margin.port.out.elastic.document.ToDoElasticDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ToDoElasticRepository extends ElasticsearchRepository<ToDoElasticDocument, String> {
}
