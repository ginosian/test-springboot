package com.margin.port.out.elastic.document;


import com.margin.port.out.elastic.document.AbstractElasticDocument;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "to_do")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ToDoElasticDocument extends AbstractElasticDocument {
    String text;
}
