package com.margin.port.out.mongo.document;

import com.margin.port.out.mongo.document.AbstractMongoDocument;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "to_do")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ToDoMongoDocument extends AbstractMongoDocument {
    String text;
}
