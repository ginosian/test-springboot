package com.margin.port.out.mongo.repository;

import com.margin.port.out.mongo.document.ToDoMongoDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ToDoMongoRepository extends MongoRepository<ToDoMongoDocument, String> {
}
