package com.margin.port.out.mongo.mapper;

import com.margin.port.out.mongo.document.ToDoMongoDocument;
import com.margin.port.out.mongo.domain.ToDoMongoDomain;
import org.springframework.stereotype.Component;

@Component
public class ToDoMongoCRUDMapper {
    public ToDoMongoDomain map(Object object){
        return new ToDoMongoDomain();
    }

    public ToDoMongoDomain map(Object object, Object object2){
        return new ToDoMongoDomain();
    }

    public ToDoMongoDocument map1(Object object){
        return new ToDoMongoDocument();
    }

    public ToDoMongoDocument map1(Object object, Object object2){
        return new ToDoMongoDocument();
    }
}
