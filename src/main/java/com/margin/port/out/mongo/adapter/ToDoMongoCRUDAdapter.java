package com.margin.port.out.mongo.adapter;

import com.margin.port.out.mongo.document.ToDoMongoDocument;
import com.margin.port.out.mongo.domain.ToDoMongoCreationDomain;
import com.margin.port.out.mongo.domain.ToDoMongoDomain;
import com.margin.port.out.mongo.domain.ToDoMongoUpdateDomain;
import com.margin.port.out.mongo.mapper.ToDoMongoCRUDMapper;
import com.margin.port.out.mongo.repository.ToDoMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ToDoMongoCRUDAdapter {

    @Autowired
    private ToDoMongoRepository toDoMongoRepository;

    @Autowired
    private ToDoMongoCRUDMapper toDoMongoCRUDMapper;

    public ToDoMongoDomain create(ToDoMongoCreationDomain toDoMongoCreationDomain){
        ToDoMongoDocument document = toDoMongoCRUDMapper.map1(toDoMongoCreationDomain);
        document = toDoMongoRepository.save(document);
        return toDoMongoCRUDMapper.map(document);
    }

    public ToDoMongoDomain update(ToDoMongoUpdateDomain toDoMongoUpdateDomain){
        ToDoMongoDocument document = toDoMongoRepository.findById(toDoMongoUpdateDomain.getId()).orElse(null);
        if(document == null){
            return null;
        }
        document = toDoMongoCRUDMapper.map1(document, toDoMongoUpdateDomain);
        document = toDoMongoRepository.save(document);
        return toDoMongoCRUDMapper.map(document);
    }

    public ToDoMongoDomain get(String userId){
        ToDoMongoDocument document = toDoMongoRepository.findById(userId).orElse(null);
        if(document == null){
            return null;
        }
        return toDoMongoCRUDMapper.map(document);
    }

    public void delete(String userId){
        toDoMongoRepository.deleteById(userId);
    }
}
