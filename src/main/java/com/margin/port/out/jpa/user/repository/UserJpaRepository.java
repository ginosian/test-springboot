package com.margin.port.out.jpa.user.repository;

import com.margin.port.out.jpa.user.entity.UserJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserJpaRepository extends JpaRepository<UserJpaEntity, Long> {
}
