package com.margin.port.out.jpa.todo.entity;

import com.margin.port.out.jpa.AbstractJpaEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "todo")
@Entity(name = "ToDoJpaEntity")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ToDoJpaEntity extends AbstractJpaEntity {
    @Column(name = "text")
    String text;
}
