package com.margin.port.out.jpa.balance.repository;

import com.margin.port.out.jpa.balance.entity.BalanceJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BalanceJpaRepository extends JpaRepository<BalanceJpaEntity, Long> {
}
