package com.margin.port.out.jpa.balance.entity;

import com.margin.port.out.jpa.AbstractJpaEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "balance")
@Entity(name = "BalanceJpaEntity")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class BalanceJpaEntity extends AbstractJpaEntity {
    @Column(name = "balance")
    Double balance;

    @Column(name = "lastInId")
    Long lastInId;

    @Column(name = "lastOutId")
    Long lastOutId;




}
