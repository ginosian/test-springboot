package com.margin.port.out.jpa.user.mapper;

import com.margin.port.out.jpa.user.domain.UserCreationJpaDomain;
import com.margin.port.out.jpa.user.domain.UserJpaDomain;
import com.margin.port.out.jpa.user.domain.UserUpdateJpaDomain;
import com.margin.port.out.jpa.user.entity.UserJpaEntity;
import org.springframework.stereotype.Component;

@Component
public class UserJpaMapper {
    public UserJpaDomain map(Object userServiceDomain){
        return new UserJpaDomain();
    }

    public UserJpaEntity map(UserCreationJpaDomain userJpaDomain){
        return new UserJpaEntity();
    }

    public UserJpaDomain map(UserJpaEntity userJpaEntity){
        return new UserJpaDomain();
    }

    public UserJpaEntity map(UserUpdateJpaDomain userJpaDomain){
        return new UserJpaEntity();
    }

    public UserJpaEntity map(UserJpaEntity userJpaEntity, UserUpdateJpaDomain userJpaDomain){
        return userJpaEntity;
    }

}
