package com.margin.port.out.jpa.balance.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BalanceJpaDomain {
    Long id;
    Double balance;
    Long lastInId;
    Long lastOutId;
}