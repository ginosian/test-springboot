package com.margin.port.out.jpa.user.adapter;

import com.margin.port.out.jpa.user.validator.UserCreationValidator;
import com.margin.port.out.jpa.user.validator.UserUpdateValidator;
import com.margin.port.out.jpa.user.domain.UserCreationJpaDomain;
import com.margin.port.out.jpa.user.domain.UserJpaDomain;
import com.margin.port.out.jpa.user.domain.UserUpdateJpaDomain;
import com.margin.port.out.jpa.user.entity.UserJpaEntity;
import com.margin.port.out.jpa.user.mapper.UserJpaMapper;
import com.margin.port.out.jpa.user.repository.UserJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserJpaCRUDAdapter {

    @Autowired
    private UserCreationValidator userCreationValidator;

    @Autowired
    private UserUpdateValidator userUpdateValidator;

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private UserJpaMapper userJpaMapper;

     public UserJpaDomain create(UserCreationJpaDomain userJpaDomain){
        userCreationValidator.validate(userJpaDomain);
        UserJpaEntity entity = userJpaMapper.map(userJpaDomain);
        entity = userJpaRepository.save(entity);
        return userJpaMapper.map(entity);
    }

    public UserJpaDomain update(UserUpdateJpaDomain userUpdateJpaDomain){
        userUpdateValidator.validate(userUpdateJpaDomain);
        UserJpaEntity entity = userJpaRepository.getById(userUpdateJpaDomain.getId());
        entity = userJpaMapper.map(entity, userUpdateJpaDomain);
        entity = userJpaRepository.save(entity);
        return userJpaMapper.map(entity);
    }

    public UserJpaDomain get(Long userId){
        UserJpaEntity entity = userJpaRepository.getById(userId);
        return userJpaMapper.map(entity);
    }

    public void delete(Long userId){
        userJpaRepository.deleteById(userId);
    }

}
