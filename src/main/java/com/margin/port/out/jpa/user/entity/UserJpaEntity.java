package com.margin.port.out.jpa.user.entity;

import com.margin.port.out.jpa.AbstractJpaEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "person")
@Entity(name = "UserJpaEntity")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class UserJpaEntity extends AbstractJpaEntity {
    @Column(name = "name")
    String name;

    @Column(name = "password")
    String password;
}
