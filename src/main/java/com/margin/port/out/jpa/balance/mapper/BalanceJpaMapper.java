package com.margin.port.out.jpa.balance.mapper;

import com.margin.port.out.jpa.balance.domain.BalanceCreationJpaDomain;
import com.margin.port.out.jpa.balance.domain.BalanceJpaDomain;
import com.margin.port.out.jpa.balance.domain.BalanceUpdateJpaDomain;
import com.margin.port.out.jpa.balance.entity.BalanceJpaEntity;
import org.springframework.stereotype.Component;

@Component
public class BalanceJpaMapper {

    public BalanceJpaDomain map(Object balanceServiceDomain){
        return new BalanceJpaDomain();
    }

    public BalanceJpaEntity map(BalanceCreationJpaDomain balanceCreationJpaDomain){
        return new BalanceJpaEntity();
    }

    public BalanceJpaDomain map(BalanceJpaEntity balanceJpaEntity){
        return new BalanceJpaDomain();
    }

    public BalanceJpaEntity map(BalanceJpaEntity balanceJpaEntity, BalanceUpdateJpaDomain balanceUpdateJpaDomain){
        return new BalanceJpaEntity();
    }
}
