package com.margin.port.out.jpa.user.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateJpaDomain {
    Long id;
    String name;
    String password; //TODO change type later
}
