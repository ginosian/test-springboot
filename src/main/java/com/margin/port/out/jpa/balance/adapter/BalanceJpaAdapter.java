package com.margin.port.out.jpa.balance.adapter;

import com.margin.port.out.jpa.balance.domain.BalanceCreationJpaDomain;
import com.margin.port.out.jpa.balance.domain.BalanceJpaDomain;
import com.margin.port.out.jpa.balance.domain.BalanceUpdateJpaDomain;
import com.margin.port.out.jpa.balance.entity.BalanceJpaEntity;
import com.margin.port.out.jpa.balance.mapper.BalanceJpaMapper;
import com.margin.port.out.jpa.balance.repository.BalanceJpaRepository;
import com.margin.port.out.jpa.balance.validator.BalanceCreationJpaValidator;
import com.margin.port.out.jpa.balance.validator.BalanceUpdateJpaValidator;
import com.margin.port.out.jpa.user.entity.UserJpaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BalanceJpaAdapter {

    @Autowired
    private BalanceCreationJpaValidator balanceCreationJpaValidator;

    @Autowired
    private BalanceUpdateJpaValidator balanceUpdateJpaValidator;

    @Autowired
    private BalanceJpaRepository balanceJpaRepository;

    @Autowired
    private BalanceJpaMapper balanceJpaMapper;

    public BalanceJpaDomain create(BalanceCreationJpaDomain balanceCreationJpaDomain){
        balanceCreationJpaValidator.validate(balanceCreationJpaDomain);
        BalanceJpaEntity entity = balanceJpaMapper.map(balanceCreationJpaDomain);
        entity = balanceJpaRepository.save(entity);
        return balanceJpaMapper.map(entity);
    }
    public BalanceJpaDomain update(BalanceUpdateJpaDomain balanceUpdateJpaDomain){
        balanceUpdateJpaValidator.validate(balanceUpdateJpaDomain);
        BalanceJpaEntity entity = balanceJpaRepository.getById(balanceUpdateJpaDomain.getId());
        entity = balanceJpaMapper.map(entity, balanceUpdateJpaDomain);
        entity = balanceJpaRepository.save(entity);
        return balanceJpaMapper.map(entity);
    }
    public BalanceJpaDomain get(Long balanceId){
        BalanceJpaEntity entity = balanceJpaRepository.getById(balanceId);
        return balanceJpaMapper.map(entity);
    }
    public void delete(Long balanceId){
        balanceJpaRepository.deleteById(balanceId);
    }
}
