package com.margin.port.out.jpa.balance.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BalanceUpdateJpaDomain {
    Long id;
    Double balance;
    Long lastInId;
    Long lastOutId;
}
