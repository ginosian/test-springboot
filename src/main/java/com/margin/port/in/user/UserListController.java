package com.margin.port.in.user;


import com.margin.port.in.user.dto.UserDTO;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "users")
public class UserListController {

    @GetMapping(path = "")
    public List<UserDTO> list(
            @RequestParam(name = "name", required = false) final String name,
            @RequestParam(name = "page", required = false) final Integer page,
            @RequestParam(name = "size", required = false) final Integer size){
        return null;
    }
}
