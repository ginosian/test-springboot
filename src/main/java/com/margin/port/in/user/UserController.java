package com.margin.port.in.user;

import com.margin.port.in.user.dto.UserCreationDTO;
import com.margin.port.in.user.dto.UserDTO;
import com.margin.port.in.user.dto.UserUpdateDTO;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "users")
public class UserController {

    @GetMapping(path = "/{userId}")
    public UserDTO get(@PathVariable(name = "userId") final Long userId){
        return null;
    }

    @PostMapping("")
    public UserDTO create(@RequestBody final UserCreationDTO userCreationDTO){
        return null;
    }

    @PutMapping(path = "/userId")
    public UserDTO update(@PathVariable(name = "userId") final Long userId,
                          @RequestBody final UserUpdateDTO userUpdateDTO){
        return null;
    }

    @DeleteMapping(path = "/{userId}")
    public void delete(@PathVariable(name = "userId") final Long userId){

    }
}
