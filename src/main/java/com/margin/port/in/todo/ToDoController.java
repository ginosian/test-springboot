package com.margin.port.in.todo;

import com.margin.application.todo.AddToDoUseCase;
import com.margin.application.todo.domain.ToDoDomain;
import com.margin.port.in.todo.dto.TodoCreationDTO;
import com.margin.port.in.todo.dto.TodoDTO;
import com.margin.port.in.todo.dto.ToDoUpdateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "users/{userId}/todos")
public class ToDoController {

    @Autowired
    private AddToDoUseCase addToDoUseCase;

    @Autowired
    private ToDoMapper toDoMapper;

    @GetMapping(path = "/{todoId}")
    public TodoDTO get(@PathVariable(name = "todoId") final Long todoId){
        return null;
    }

    @PostMapping("")
    public TodoDTO create(@RequestBody final TodoCreationDTO todoCreationDTO){
        ToDoDomain toDoDomain = addToDoUseCase.add(toDoMapper.map(todoCreationDTO));
        return toDoMapper.map(toDoDomain);
    }

    @PutMapping(path = "/todoId")
    public TodoDTO update(@PathVariable(name = "todoId") final Long todoId,
                          @RequestBody final ToDoUpdateDTO toDoUpdateDTO){
        return null;
    }

    @DeleteMapping(path = "/{todoId}")
    public void delete(@PathVariable(name = "todoId") final Long todoId){

    }
}
