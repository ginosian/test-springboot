package com.margin.port.in.todo;

import com.margin.application.todo.domain.AddToDoDomain;
import com.margin.application.todo.domain.ToDoDomain;
import com.margin.port.in.todo.dto.TodoCreationDTO;
import com.margin.port.in.todo.dto.TodoDTO;
import org.springframework.stereotype.Component;

@Component
public class ToDoMapper {

    AddToDoDomain map(TodoCreationDTO todoCreationDTO){
        return new AddToDoDomain();
    }

    TodoDTO map(ToDoDomain toDoDomain){
        return new TodoDTO();
    }
}
