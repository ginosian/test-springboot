package com.margin.port.in.todo;


import com.margin.port.in.todo.dto.TodoDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "todos")
public class ToDoListController {

    @GetMapping(path = "")
    public List<TodoDTO> list(
            @RequestParam(name = "username", required = false) final String username,
            @RequestParam(name = "text", required = false) final String name,
            @RequestParam(name = "page", required = false) final Integer page,
            @RequestParam(name = "size", required = false) final Integer size){
        return null;
    }
}
