package com.margin.port.in.todo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ToDoUpdateDTO {
    Long id;
    Long userId;
    String text;
}
