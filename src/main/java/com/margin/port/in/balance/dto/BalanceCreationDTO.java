package com.margin.port.in.balance.dto;

import com.margin.infrastructure.Currency;
import com.margin.infrastructure.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BalanceCreationDTO {
    TransactionType transactionType;
    String userId;
    Currency currency;
    BigDecimal sum;
}
