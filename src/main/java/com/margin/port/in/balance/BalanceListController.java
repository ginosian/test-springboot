package com.margin.port.in.balance;

import com.margin.infrastructure.Currency;
import com.margin.infrastructure.TransactionType;
import com.margin.port.in.balance.dto.BalanceDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "balances")
public class BalanceListController {
    @GetMapping(path = "")
     public List<BalanceDTO> list(@RequestParam(name = "userId", required = false) final Long userId,
                                  @RequestParam(name = "currency", required = false) final Currency currency,
                                  @RequestParam(name = "transactionType", required = false) final TransactionType transactionType,
                                  @RequestParam(name = "page", required = false) final Integer page,
                                  @RequestParam(name = "size", required = false) final  Integer size) {
        return  null;
    }
}
