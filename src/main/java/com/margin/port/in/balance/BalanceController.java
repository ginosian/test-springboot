package com.margin.port.in.balance;

import com.margin.infrastructure.Currency;
import com.margin.infrastructure.TransactionType;
import com.margin.port.in.balance.dto.BalanceCreationDTO;
import com.margin.port.in.balance.dto.BalanceDTO;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping(path = "balances")
public class BalanceController {
    @GetMapping(path = "/{balanceId}")
    public BalanceDTO get(@PathVariable(name = "balanceId") final Long balanceId){
        return new BalanceDTO(TransactionType.IN, "lilia", new BigDecimal(1000), Currency.AMD,balanceId );
    }

    @PostMapping(path = "")
    public BalanceDTO create(@RequestBody final BalanceCreationDTO balanceCreationDTO){
        return  null;
    }
}
