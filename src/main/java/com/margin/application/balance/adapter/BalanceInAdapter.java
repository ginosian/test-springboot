package com.margin.application.balance.adapter;

import com.margin.application.balance.domain.GetBalanceDomain;
import com.margin.port.in.balance.dto.BalanceCreationDTO;
import com.margin.port.in.balance.dto.BalanceDTO;
import org.springframework.stereotype.Component;

@Component
public class BalanceInAdapter {

    GetBalanceDomain map(BalanceDTO balanceDTO){
        return new GetBalanceDomain();
    }

    BalanceDTO map(GetBalanceDomain domain){
        return new BalanceDTO();
    }

}
