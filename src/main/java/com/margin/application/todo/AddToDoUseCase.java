package com.margin.application.todo;

import com.margin.application.todo.adapter.ToDoOutAdapter;
import com.margin.application.todo.domain.AddToDoDomain;
import com.margin.application.todo.domain.ToDoDomain;
import com.margin.port.out.mongo.adapter.ToDoMongoCRUDAdapter;
import com.margin.port.out.mongo.document.ToDoMongoDocument;
import com.margin.port.out.mongo.domain.ToDoMongoCreationDomain;
import com.margin.port.out.mongo.domain.ToDoMongoDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddToDoUseCase {

    @Autowired
    private ToDoMongoCRUDAdapter toDoMongoCRUDAdapter;

    @Autowired
    private ToDoOutAdapter toDoOutAdapter;

    public ToDoDomain add(AddToDoDomain addToDoDomain){
        ToDoMongoCreationDomain outDomain =  toDoOutAdapter.map(addToDoDomain);
        ToDoMongoDomain toDoMongoDocument = toDoMongoCRUDAdapter.create(outDomain);
        ToDoDomain toDoDomain = toDoOutAdapter.map(toDoMongoDocument);
        return toDoDomain;
    }


}
