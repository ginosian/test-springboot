package com.margin.application.todo.adapter;

import com.margin.application.todo.domain.AddToDoDomain;
import com.margin.application.todo.domain.UpdateToDoDomain;
import com.margin.port.in.todo.dto.ToDoUpdateDTO;
import com.margin.port.in.todo.dto.TodoCreationDTO;
import org.springframework.stereotype.Component;

@Component
public class ToDoInAdapter {

    AddToDoDomain map(TodoCreationDTO todoCreationDTO){
        return new AddToDoDomain();
    }

    UpdateToDoDomain map(ToDoUpdateDTO toDoUpdateDTO){
        return new UpdateToDoDomain();
    }



}
