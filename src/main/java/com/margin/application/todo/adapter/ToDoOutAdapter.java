package com.margin.application.todo.adapter;

import com.margin.application.todo.domain.AddToDoDomain;
import com.margin.application.todo.domain.ToDoDomain;
import com.margin.port.out.jpa.user.domain.UserCreationJpaDomain;
import com.margin.port.out.mongo.domain.ToDoMongoCreationDomain;
import com.margin.port.out.mongo.domain.ToDoMongoDomain;
import org.springframework.stereotype.Component;

@Component
public class ToDoOutAdapter {

    public ToDoMongoCreationDomain map(AddToDoDomain addToDoDomain){
        return new ToDoMongoCreationDomain();
    }

    public ToDoDomain map(ToDoMongoDomain toDoMongoDomain){
        return new ToDoDomain();
    }

}
