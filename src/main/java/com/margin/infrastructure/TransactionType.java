package com.margin.infrastructure;

public enum TransactionType {
    IN,
    OUT
}
