package com.margin.listener;

import com.margin.port.out.elastic.document.ToDoElasticDocument;
import com.margin.port.out.elastic.repository.ToDoElasticRepository;
import com.margin.port.out.jpa.user.entity.UserJpaEntity;
import com.margin.port.out.jpa.user.repository.UserJpaRepository;
import com.margin.port.out.mongo.document.ToDoMongoDocument;
import com.margin.port.out.mongo.repository.ToDoMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationEventListener {

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private ToDoMongoRepository toDoMongoRepository;

    @Autowired
    private ToDoElasticRepository toDoElasticRepository;

    @EventListener(ContextRefreshedEvent.class)
    public void onContextRefreshedEvent() {
        userJpaRepository.save(new UserJpaEntity("Lilia", "111"));
        toDoMongoRepository.save(new ToDoMongoDocument("Lilia"));
        toDoElasticRepository.save(new ToDoElasticDocument("Lilia"));
    }


}
